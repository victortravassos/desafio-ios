//
//  AppDelegate.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 04/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }


}

