//
//  Owner.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit
import SwiftyJSON


class Owner {
    var id:Int?
    var username:String?
    var avatarUrl:NSURL?
    
    init(json:JSON) {
        if let id = json["id"].int {
            self.id = id
        }
        if let username = json["login"].string {
            self.username = username
        }
        if let avatarUrl = json["avatar_url"].string {
            self.avatarUrl = NSURL(string: avatarUrl)
        }
    }
    
}
