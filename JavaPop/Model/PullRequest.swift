import UIKit
import SwiftyJSON

class PullRequest {
    
    var title: String?
    var owner:Owner?
    var body:String?
    var createdAt:NSDate?
    var url:String?
    
    init(json: JSON) {
        
        if let title = json["title"].string {
            self.title = title
        }
        
        self.owner = Owner(json: json["user"])
        
        if let body = json["body"].string {
            self.body = body
        }
        if let createdAt = json["created_at"].string {
            self.createdAt = ParserUtils.stringToDate(createdAt, format: SERVER_DATE_FORMAT)
        }
        
        if let url = json["html_url"].string {
            self.url = url
        }
        
    }
    
}
