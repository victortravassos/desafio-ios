//
//  Repository.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit
import SwiftyJSON

class Repository {

    var name: String?
    var owner:Owner?
    var description:String?
    var starsCount:Int?
    var forksCount:Int?
    
    init(json: JSON) {
        
        if let name = json["name"].string {
            self.name = name
        }
        
        self.owner = Owner(json: json["owner"])
        
        if let description = json["description"].string {
            self.description = description
        }
        if let starsCount = json["stargazers_count"].int {
            self.starsCount = starsCount
        }
        if let forksCount = json["forks_count"].int {
            self.forksCount = forksCount
        }

    }
    
}
