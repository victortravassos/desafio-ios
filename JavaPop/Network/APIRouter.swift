//
//  APIRouter.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import Foundation
import Alamofire


enum APIRouter: URLRequestConvertible{
    static let baseURLString = "https://api.github.com"

    case GetRepositories(Int)
    case GetPullRequests(Repository)
    
    var URLRequest: NSMutableURLRequest {

        var method: Alamofire.Method {
            switch self {
                case .GetRepositories:
                    return .GET
                case .GetPullRequests:
                    return .GET
            }
        }
        
        let url:NSURL = {
            let relativePath:String?
            let urlComponent = NSURLComponents(string: APIRouter.baseURLString)
            
            switch self {
                case .GetRepositories(let pageNumber):
                    relativePath = "/search/repositories"
                    
                    let queryJava = NSURLQueryItem(name: "q", value: "language:Java")
                    let querySort = NSURLQueryItem(name: "sort", value: "stars")
                    let queryPages = NSURLQueryItem(name: "page", value: "\(pageNumber)")
                    urlComponent?.queryItems = [queryJava,querySort,queryPages]
                    
                case .GetPullRequests(let repository):
                        relativePath = "/repos/\(repository.owner!.username!)/\(repository.name!)/pulls"
            }
            
            urlComponent?.path = relativePath
            
            return (urlComponent?.URL)!
        
        }()
        
        let URLRequest = NSMutableURLRequest(URL: url)
        
        let encoding = Alamofire.ParameterEncoding.JSON
        let (encodedRequest, _) = encoding.encode(URLRequest, parameters: nil)
        
        encodedRequest.HTTPMethod = method.rawValue
        
        return encodedRequest
        
    }
}
