//
//  PullRequestsManager.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PullRequestsManager {
    
    class var sharedInstance: PullRequestsManager {
        struct Singleton {
            static let instance : PullRequestsManager = PullRequestsManager()
        }
        return Singleton.instance
    }
    
    let manager = Manager.sharedInstance
    
    private init() {}
    
    func getPullRequests(repository: Repository, completion:(_:[PullRequest]) -> ()) -> Request {
        return manager.request(APIRouter.GetPullRequests(repository)).validate().responseJSON { response in
            switch response.result {
                case .Success(let value):
                    let json = JSON(value)
                    let itemsList: Array<JSON> = json.arrayValue
                    var prArray:[PullRequest] = []
                    for (item) in itemsList {
                        prArray.append(PullRequest(json: item))
                    }
                    completion(prArray)
                    break
                    
                case .Failure(let error):
                    print(error)
                    break
            }
        }
    }
}
