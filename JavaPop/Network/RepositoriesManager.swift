//
//  APIManager.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RepositoriesManager {
    
    class var sharedInstance: RepositoriesManager {
        struct Singleton {
            static let instance : RepositoriesManager = RepositoriesManager()
        }
        return Singleton.instance
    }
    
    let manager = Manager.sharedInstance
    
    private init() {}
    
    func getRepositories(page:Int, completion:(_:[Repository]) -> ()) -> Request {
        return manager.request(APIRouter.GetRepositories(page)).validate().responseJSON { response in
            switch response.result {
                case .Success(let value):
                    let json = JSON(value)
                    let itemsList: Array<JSON> = json["items"].arrayValue
                    var repoArray:[Repository] = []
                    for (item) in itemsList {
                        repoArray.append(Repository(json: item))
                    }
                    completion(repoArray)
                    break
                
                case .Failure(let error):
                    print(error)
                    break
            }
        }
    }

}
