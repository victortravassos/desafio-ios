//
//  ParserUtils.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import Foundation

let SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
let DEFAULT_DATE_FORMAT = "dd/MM/yyyy"

class ParserUtils {
    
    class func stringToDate(dateString:String, format:String) -> NSDate? {
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.dateFromString(dateString)
    }
    
    class func dateToString(date:NSDate, format:String) -> String? {
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.stringFromDate(date)
    }
}
