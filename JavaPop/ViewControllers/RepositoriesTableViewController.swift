//
//  RepositoriesTableViewController.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 04/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit
import SDWebImage
import PKHUD


class RepositoriesTableViewController: UITableViewController {
    let reuseIdentifier = "repositoryItem"
    let segueIdentifier = "repositoryPRSegue"
    let repositoriesManager = RepositoriesManager.sharedInstance
    var repositoriesList:[Repository] = []
    var pageNumber = 1
    var selectedRepository: Repository?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadRepositories(pageNumber)
    }
    
    func loadRepositories(page:Int) {
        HUD.show(.Progress)
        repositoriesManager.getRepositories(page) { (data) in
            if let repoList = data as [Repository]? {
                self.repositoriesList.appendContentsOf(repoList)
                self.tableView.reloadData()
                self.pageNumber += 1
                HUD.hide(animated: true)
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoriesList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! RepoTableViewCell
        let repository = repositoriesList[indexPath.row]
        cell.nameLabel.text = repository.name
        cell.usernameLabel.text = repository.owner?.username
        cell.descriptionLabel.text = repository.description
        cell.forksCountLabel.text = "\(repository.forksCount!)"
        cell.starsCountLabel.text = "\(repository.starsCount!)"
        cell.avatarImage.sd_setImageWithURL(repository.owner?.avatarUrl, placeholderImage: UIImage(named: "avatar"))
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedRepository = repositoriesList[indexPath.row]
        self.performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == segueIdentifier {
            let destVC = segue.destinationViewController as! RepositoryDetailTableViewController
            destVC.currentRepository = selectedRepository
        }
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.alpha = 0.0
        let animationTransform = CATransform3DTranslate(CATransform3DIdentity, -200, 10, 0)
        cell.layer.transform = animationTransform
        UIView.animateWithDuration(0.5) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity

        }
        
        if (indexPath.row == repositoriesList.count - 1) {
            loadRepositories(pageNumber)
        }
        
    }

}
