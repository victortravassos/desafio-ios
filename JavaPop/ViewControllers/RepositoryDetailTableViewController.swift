//
//  RepositoryDetailTableViewController.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 04/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage

class RepositoryDetailTableViewController: UITableViewController {
    
    let reuseIdentifier = "pullRequestItem"
    let pullRequestsManager = PullRequestsManager.sharedInstance
    var pullRequestsList:[PullRequest] = []
    var currentRepository:Repository?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPullRequests()
        self.title = currentRepository?.name

    }

    func loadPullRequests() {
        HUD.show(.Progress)
        if currentRepository != nil {
            pullRequestsManager.getPullRequests(currentRepository!, completion: {  [unowned self] pullRequests in
                if let data = pullRequests as [PullRequest]? {
                    self.pullRequestsList = data
                    self.tableView.reloadData()
                    HUD.hide(animated: true)
                }
            })
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        let imageCache = SDImageCache.sharedImageCache()
        imageCache.clearMemory()
        imageCache.clearDisk()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PullRequestItemTableViewCell
        
        let pullrequest = pullRequestsList[indexPath.row]
        
        cell.titleLabel.text = pullrequest.title!
        cell.descriptionLabel.text = pullrequest.body!
        cell.username.text = pullrequest.owner?.username
        cell.dateLabel.text = ParserUtils.dateToString(pullrequest.createdAt!, format: DEFAULT_DATE_FORMAT)
        cell.avatarImageView.sd_setImageWithURL(pullrequest.owner?.avatarUrl!, placeholderImage: UIImage(named: "avatar"))
        
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequestsList.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedPullRequest = pullRequestsList[indexPath.row]
        UIApplication.sharedApplication().openURL(NSURL(string: selectedPullRequest.url!)!)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.alpha = 0.0
        let animationTransform = CATransform3DTranslate(CATransform3DIdentity, -200, 10, 0)
        cell.layer.transform = animationTransform
        UIView.animateWithDuration(0.5) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
            
        }
    }
}
