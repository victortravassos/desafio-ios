//
//  PullRequestItemTableViewCell.swift
//  JavaPop
//
//  Created by Victor Renan Ferreira on 05/12/16.
//  Copyright © 2016 Victor Renan Ferreira. All rights reserved.
//

import UIKit

class PullRequestItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

}
